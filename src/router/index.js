import Vue from 'vue'
import Router from 'vue-router'
import ThemeAudience from '@/themes/theme-audience'
import ThemeIndividual from '@/themes/theme-individual'
import ThemeCorporate from '@/themes/theme-corporate'
import Error from '@/components/InnerPages/Error/404'
import SignUp from '@/components/Accounts/SignUp/signup'
import Contact from '@/components/InnerPages/ContactPage/contact'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [

    {
      path: '/',
      name: 'ThemeAudience',
      component: ThemeAudience
    },
    {
    path: '/theme-individual',
    name: 'ThemeIndividual',
    component: ThemeIndividual
    },
    {
      path: '/theme-corporate',
      name: 'ThemeCorporate',
      component: ThemeCorporate
      },
    {
      path: '/404',
      name: 'Error',
      component: Error
    },
    {
      path: '/signup',
      name: 'SignUp',
      component: SignUp
    },
    {
      path: '/contact',
      name: 'Contact',
      component: Contact
    }
  ]
})